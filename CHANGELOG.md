Change-Log
===============
> Regular maintenance: _27.04.2023_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 3.x ##

### [3.0.1](https://bitbucket.org/android-universum/testing/wiki/version/3.x) ###
> 27.04.2023

- Bumped **compile SDK version** to `32`.
- Bumped **Gradle** version to `7.6.1`.
- Bumped **AGP** version to `7.4.2`.
- Bumped **dependencies** to latest versions.

### [3.0.0](https://bitbucket.org/android-universum/testing/wiki/version/3.x) ###
> 12.06.2021

- New **group id**.

## [Version 2.x](https://bitbucket.org/android-universum/testing/wiki/version/2.x) ##
## [Version 1.x](https://bitbucket.org/android-universum/testing/wiki/version/1.x) ##
