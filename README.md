Android Testing
===============

[![CircleCI](https://circleci.com/bb/android-universum/testing.svg?style=shield)](https://circleci.com/bb/android-universum/testing)
[![Codecov](https://codecov.io/bb/android-universum/testing/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/testing)
[![Codacy](https://api.codacy.com/project/badge/Grade/c43a592ec54a4d6caf0f8c2f2647a33e)](https://app.codacy.com/bb/android-universum/testing/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Android](https://img.shields.io/badge/android-13.0-blue.svg)](https://developer.android.com/about/versions/13)
[![Kotlin](https://img.shields.io/badge/kotlin-1.7.21-blue.svg)](https://kotlinlang.org)
[![Robolectric](https://img.shields.io/badge/robolectric-4.9.2-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Test cases and testing utilities for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/testing/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/testing/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "io.bitbucket.android-universum:testing:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/testing/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`junit:junit`](https://junit.org/junit4)
- [`org.robolectric:robolectric`](http://robolectric.org)
- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.legacy:legacy-support-v4`](https://developer.android.com/jetpack/androidx)
- [`androidx.test:core`](https://developer.android.com/jetpack/androidx)
- [`androidx.test.ext:junit`](https://developer.android.com/jetpack/androidx)

## [License](https://bitbucket.org/android-universum/testing/src/main/LICENSE.md) ##

**Copyright 2023 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.

See the License for the specific language governing permissions and limitations under the License.
