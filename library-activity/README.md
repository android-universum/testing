Testing-Activity
===============

This module contains simple `Activity` implementations that may be used in **Android tests**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atesting/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atesting/_latestVersion)

### Gradle ###

    compile "universum.studios.android:testing-activity:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [TestActivity](https://bitbucket.org/android-universum/testing/src/main/library-activity/src/main/java/universum/studios/android/test/TestActivity.kt)
