package universum.studios.android.testing

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class TestActivityInstrumentedTest : AndroidTestCase() {

	@Test fun perform() {
		// Act + Assert:
        MatcherAssert.assertThat(context, Is.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(context, Is.`is`(ApplicationProvider.getApplicationContext<Context>()))
	}
}