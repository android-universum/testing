package universum.studios.android.testing

import android.content.Intent
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.annotation.NonNull
import androidx.annotation.StyleRes
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.FragmentActivity

/**
 * Simple [FragmentActivity] that may be used in **Android tests**.
 *
 * If [TestActivity] is desired to be launched with a custom theme, an [Intent] created via
 * [TestActivity.createIntent] with that theme resource should be passed to the activity as
 * its extras intent.
 *
 * This activity by default uses an instance of [FrameLayout] as its content view.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class TestActivity : FragmentActivity() {

    /**
     */
    companion object {

        /**
         * Style resource of the default theme used by the [TestActivity].
         */
        @JvmStatic @StyleRes
        val THEME_STYLE =
            if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) android.R.style.Theme_Material_Light
            else android.R.style.Theme_Light

        /**
         * Id of the [TestActivity]'s content view.
         */
        @IdRes const val CONTENT_VIEW_ID = android.R.id.custom

        /**
         * Extra key used to provide custom theme style resource for the [TestActivity] via [Intent].
         */
        @VisibleForTesting internal val EXTRA_THEME = "${TestActivity::class.java.name}.EXTRA.Theme"

        /**
         * Creates a new [Intent] for the [TestActivity] with the given [theme] style resource.
         *
         * **Note that this intent is not suited to be used to start [TestActivity] activity,
         * because it will only contain extras for the activity not the activity component necessary
         * to start the activity.**
         *
         * @param theme Style resource of the desired theme which should be applied to the
         *              [TestActivity] when created.
         * @return Intent with the theme resource presented.
         */
        @JvmStatic @NonNull
        fun createIntent(@StyleRes theme: Int): Intent = Intent().apply { putExtra(EXTRA_THEME, theme) }
    }

    /*
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(intent.getIntExtra(EXTRA_THEME, THEME_STYLE))

        setContentView(
            FrameLayout(this).apply { id = CONTENT_VIEW_ID }
        )
    }

    /**
     * Returns the content view of this activity.
     *
     * This is the same as calling [TestActivity.findViewById(TestActivity.CONTENT_VIEW_ID)][TestActivity.findViewById].
     *
     * @return This activity's content view.
     */
    @NonNull fun <V : View> contentView(): V = findViewById(CONTENT_VIEW_ID)
}