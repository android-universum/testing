package universum.studios.android.testing

import android.R.id
import android.R.style
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.view.View
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.robolectric.Robolectric

/**
 * @author Martin Albedinsky
 */
class TestActivityTest : AndroidTestCase() {

    @Test fun contract() {
        // Assert:
        MatcherAssert.assertThat(
            TestActivity.THEME_STYLE, CoreMatchers.`is`(
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) style.Theme_Material_Light
                else style.Theme_Light
            )
        )
        MatcherAssert.assertThat(TestActivity.CONTENT_VIEW_ID, CoreMatchers.`is`(id.custom))
        MatcherAssert.assertThat(TestActivity.EXTRA_THEME, CoreMatchers.`is`("${TestActivity::class.java.name}.EXTRA.Theme"))
    }

    @Test fun createIntent() {
        // Act:
        val intent = TestActivity.createIntent(style.Theme_Black)

        // Assert:
        MatcherAssert.assertThat(intent, CoreMatchers.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(intent.extras?.size(), CoreMatchers.`is`(1))
        MatcherAssert.assertThat(intent.getIntExtra(TestActivity.EXTRA_THEME, -1), CoreMatchers.`is`(style.Theme_Black))
    }

    @Test fun contentView() {
        // Arrange:
        val activity = Robolectric.buildActivity(TestActivity::class.java).create().get()

        // Act:
        val contentView = activity.contentView<View>()

        // Assert:
        MatcherAssert.assertThat(contentView, CoreMatchers.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(contentView, CoreMatchers.`is`(activity.findViewById<View>(TestActivity.CONTENT_VIEW_ID)))
    }
}