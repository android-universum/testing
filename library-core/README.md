Testing-Core
===============

This module contains core test cases.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atesting/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atesting/_latestVersion)

### Gradle ###

    compile "universum.studios.android:testing-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [TestCase](https://bitbucket.org/android-universum/testing/src/main/library-core/src/main/java/universum/studios/android/test/TestCase.kt)
- [AndroidTestCase](https://bitbucket.org/android-universum/testing/src/main/library-core/src/main/java/universum/studios/android/test/AndroidTestCase.kt)
