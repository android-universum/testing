package universum.studios.android.testing

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.runner.RunWith

/**
 * Test case class that may be used to group **suite of Android tests** that are to be executed
 * either on a local *JVM* with shadowed *Android environment* or on an emulated/real *Android Device*.
 *
 * **Default runner:** [AndroidJUnit4]
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@RunWith(AndroidJUnit4::class)
abstract class AndroidTestCase : TestCase {

    /**
     * Lazy application context instance obtained via [ApplicationProvider.getApplicationContext].
     */
    protected val context: Context by lazy { ApplicationProvider.getApplicationContext() }
}