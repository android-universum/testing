package universum.studios.android.testing

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class AndroidTestCaseTest : AndroidTestCase() {

	@Test fun perform() {
		// Act + Assert:
        MatcherAssert.assertThat(context, CoreMatchers.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(context, CoreMatchers.`is`(ApplicationProvider.getApplicationContext<Context>()))
	}
}