Testing-Kotlin
===============

This module contains utilities specific for tests written in _Kotlin_ language.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atesting/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atesting/_latestVersion)

### Gradle ###

    compile "universum.studios.android:testing-kotlin:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [KotlinArgumentMatchers](https://bitbucket.org/android-universum/testing/src/main/library-kotlin/src/main/java/universum/studios/android/test/kotlin/KotlinArgumentMatchers.kt)
- [KotlinArgumentCaptor](https://bitbucket.org/android-universum/testing/src/main/library-kotlin/src/main/java/universum/studios/android/test/kotlin/KotlinArgumentCaptor.kt)
