/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.kotlin

import org.mockito.ArgumentCaptor

/**
 * Utility class providing *Kotlin* specific, or rather compatible, argument matchers.
 *
 * @author Martin Albedinsky
 * @since 1.1
 */
@Suppress("UNCHECKED_CAST")
object KotlinArgumentCaptor {

    /**
     * Delegates to [ArgumentCaptor.forClass].
     *
     * @param clazz The desired type matching the parameter to be captured.
     * @param <S> Type of the given clazz.
     * @param <U> Type of the object captured by the newly built captor.
     * @return New argument captor ready to be used.
     */
    fun <U, S : U> captor(clazz: Class<S>): ArgumentCaptor<S> = ArgumentCaptor.forClass(clazz)

    /**
     * Delegates to [ArgumentCaptor.forClass] with [List<S>][List] class.
     *
     * @param <S> Type of the element in the list.
     * @param <U> Type of the object captured in the list by the newly built captor.
     * @return New argument captor ready to be used for list argument capturing.
     */
    fun <U, S : U> listCaptor(): ArgumentCaptor<List<S>> = ArgumentCaptor.forClass(List::class.java) as ArgumentCaptor<List<S>>

    /**
     * Delegates to [ArgumentCaptor.capture] and returns the given [empty] value.
     *
     * @param empty Empty value to be returned by this function due to Kotlin language compatibility.
     * @param <T> Type of the object to be captured by the captor.
     * @return Given empty value.
     */
    fun <T> capture(captor: ArgumentCaptor<T>, empty: T): T {
        captor.capture()
        return empty
    }
}