/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.1
 */
package universum.studios.android.testing.kotlin

import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Creates a new mock object of the class or interface specified as type for this function.
 *
 * @return Mock object of the desired type.
 */
inline fun <reified T> typedMock(): T = mock(T::class.java)

/**
 * Delegates to [Mockito.any] and returns uninitialized instance of the desired type.
 *
 * @param T Accepted type.
 * @return Uninitialized instance of the type.
 */
fun <T> any(): T = KotlinArgumentMatchers.any()

/**
 * Delegates to [Mockito.any] and returns uninitialized instance of the desired type.
 *
 * @param type Class of the accepted type.
 * @param T Accepted type.
 * @return Uninitialized instance of the type.
 */
fun <T> any(type: Class<T>): T = KotlinArgumentMatchers.any(type)

/**
 * Delegates to [Mockito.eq] and returns uninitialized instance of the desired type.
 *
 * @param value The given value.
 * @param T Type of the given value.
 * @return Uninitialized instance of the the type.
 */
fun <T> eq(value: T): T = KotlinArgumentMatchers.eq(value)

/**
 * Creates a new `ArgumentCaptor` object for the class or interface specified as type for this function.
 *
 * @return New argument captor ready to be used.
 *
 * @see captor
 */
inline fun <reified T> typedCaptor(): ArgumentCaptor<T> = captor(T::class.java)

/**
 * Delegates to [KotlinArgumentCaptor.captor].
 *
 * @param clazz The desired type matching the parameter to be captured.
 * @param <S> Type of the given clazz.
 * @param <U> Type of the object captured by the newly built captor.
 *
 * @return New argument captor ready to be used.
 */
fun <U, S : U> captor(clazz: Class<S>): ArgumentCaptor<S> = KotlinArgumentCaptor.captor<U, S>(clazz)

/**
 * Delegates to [KotlinArgumentCaptor.listCaptor] with [List<S>][List] class.
 *
 * @param <S> Type of the element in the list.
 * @param <U> Type of the object captured in the list by the newly built captor.
 *
 * @return New argument captor ready to be used for list argument capturing.
 */
fun <U, S : U> listCaptor(): ArgumentCaptor<List<S>> = KotlinArgumentCaptor.listCaptor<U, S>()

/**
 * Delegates to [KotlinArgumentCaptor.capture] and returns the given [empty] value.
 *
 * @param empty Empty value to be returned by this function due to Kotlin language compatibility.
 * @param <T> Type of the object to be captured by the captor.
 * @return Given empty value.
 */
@Suppress("UNCHECKED_CAST")
fun <T> capture(captor: ArgumentCaptor<T>, empty: T = null as T): T = KotlinArgumentCaptor.capture(captor, empty)