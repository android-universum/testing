package universum.studios.android.testing.kotlin

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class KotlinArgumentCaptorTest : TestCase {

	@Test fun capture() {
		// Arrange:
		val data = TestData("test", 100)
		val mockAction = Mockito.mock(TestAction::class.java)
		val handler = TestHandler(mockAction)

		// Act + Assert:
		handler.handle(data)
		val dataCaptor = captor(TestData::class.java)
		Mockito.verify(mockAction).perform(universum.studios.android.testing.kotlin.capture(dataCaptor))
        MatcherAssert.assertThat(dataCaptor.value.first, CoreMatchers.`is`(data.first))
        MatcherAssert.assertThat(dataCaptor.value.second, CoreMatchers.`is`(data.second))
        Mockito.verifyNoMoreInteractions(mockAction)
	}

	@Test fun captureList() {
		// Arrange:
		val dataFirst = TestData("first", 100)
		val dataSecond = TestData("second", 200)
		val mockAction = Mockito.mock(TestAction::class.java)
		val handler = TestHandler(mockAction)

		// Act + Assert:
		handler.handle(listOf(dataFirst, dataSecond))
		val dataCaptor: ArgumentCaptor<List<TestData>> = listCaptor()
		Mockito.verify(mockAction).perform(universum.studios.android.testing.kotlin.capture(dataCaptor))
        MatcherAssert.assertThat(dataCaptor.value[0].first, CoreMatchers.`is`(dataFirst.first))
        MatcherAssert.assertThat(dataCaptor.value[0].second, CoreMatchers.`is`(dataFirst.second))
        MatcherAssert.assertThat(dataCaptor.value[1].first, CoreMatchers.`is`(dataSecond.first))
        MatcherAssert.assertThat(dataCaptor.value[1].second, CoreMatchers.`is`(dataSecond.second))
        Mockito.verifyNoMoreInteractions(mockAction)
	}

	private class TestData(var first: String = "", var second: Int = 0)

	private interface TestAction {

		fun perform(data: TestData)

		fun perform(data: List<TestData>)
	}

	private class TestHandler(private val action: TestAction) {

		fun handle(data: TestData) = action.perform(data)

		fun handle(data: List<TestData>) = action.perform(data)
	}
}