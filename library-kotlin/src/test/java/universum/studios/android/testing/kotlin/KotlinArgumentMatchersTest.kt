/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.kotlin

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class KotlinArgumentMatchersTest : TestCase {

	@Test fun any() {
		// Arrange:
		val data = TestData()
		val mockInterface = mock(TestInterface::class.java)
		`when`(mockInterface.format(data)).thenReturn("formatted")

		// Act + Assert:
		assertThat(mockInterface.format(data), `is`("formatted"))
		verify(mockInterface).format(KotlinArgumentMatchers.any())
		verifyNoMoreInteractions(mockInterface)
	}

	@Test fun anyWithType() {
		// Arrange:
		val data = TestData()
		val mockInterface = mock(TestInterface::class.java)
		`when`(mockInterface.format(data)).thenReturn("formatted")

		// Act + Assert:
		assertThat(mockInterface.format(data), `is`("formatted"))
		verify(mockInterface).format(KotlinArgumentMatchers.any(TestData::class.java))
		verifyNoMoreInteractions(mockInterface)
	}

	@Test fun eq() {
		// Arrange:
		val data = TestData()
		val mockInterface = mock(TestInterface::class.java)
		`when`(mockInterface.format(data)).thenReturn("formatted")

		// Act + Assert:
		assertThat(mockInterface.format(data), `is`("formatted"))
		verify(mockInterface).format(KotlinArgumentMatchers.eq(data))
		verifyNoMoreInteractions(mockInterface)
	}

	private class TestData
	private interface TestInterface {

		fun format(data: TestData): String
	}
}