/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.kotlin

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.mockingDetails
import org.mockito.internal.progress.ThreadSafeMockingProgress.mockingProgress
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class KotlinTest : TestCase {

    @After fun afterTest() {
        mockingProgress().argumentMatcherStorage.reset()
    }

    @Test fun typedMock() {
        // Act:
        val first = universum.studios.android.testing.kotlin.typedMock<TestTypedInterface<String>>()
        val second = universum.studios.android.testing.kotlin.typedMock<TestTypedInterface<Boolean>>()

        // Assert:
        val firstMockingDetails = mockingDetails(first)
        val secondMockingDetails = mockingDetails(second)
        assertTrue(firstMockingDetails.isMock)
        assertTrue(secondMockingDetails.isMock)
    }

    @Test fun anyImplementation() {
        // Act + Assert:
        assertThat(any(), `is`(nullValue()))
    }

    @Test fun anyWithType() {
        // Act + Assert:
        assertThat(any(TestClass::class.java), `is`(nullValue()))
    }

    @Test fun eq() {
        // Act + Assert:
        assertThat(universum.studios.android.testing.kotlin.eq(TestClass::class.java), `is`(nullValue()))
    }

    @Test fun typedCaptorImplementation() {
        // Act + Assert:
        assertThat(typedCaptor<TestClass>(), `is`(notNullValue()))
        assertThat(typedCaptor<TestTypedInterface<String>>(), `is`(notNullValue()))
    }

    @Test fun captor() {
        // Act + Assert:
        assertThat(universum.studios.android.testing.kotlin.captor(TestClass::class.java), `is`(notNullValue()))
    }

    @Test fun listCaptorImplementation() {
        // Act:
        val captor: ArgumentCaptor<List<TestClass>> = listCaptor()

        // Assert:
        assertThat(captor, `is`(notNullValue()))
    }

    private class TestClass

    @Suppress("unused")
    private interface TestTypedInterface<T>
}