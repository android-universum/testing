package universum.studios.android.testing.matcher

import androidx.annotation.NonNull
import org.hamcrest.Matcher

/**
 * Convenience factory that may be used to create **class declaration** related matchers.
 *
 * @author Martin Albedinsky
 * @since 1.1
 */
object DeclarationMatchers {

    /**
     * Creates a matcher that matches if examined object implements the specified interface [clazz].
     *
     * **Note that the specified [clazz] must be actually an interface and not a simple class,
     * otherwise an exception is thrown.**
     *
     * For example:
     *
     * `assertThat(object, implements(SomeInterface::class))`
     *
     * @param clazz The desired interface class to expect.
     * @param T Type of the object to be examined.
     * @return Matcher ready to be used.
     */
    @NonNull @JvmStatic
    fun <T> implements(@NonNull clazz: Class<*>): Matcher<T> = ImplementsMatcher.implements(clazz)
}