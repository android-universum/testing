/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.matcher

import androidx.annotation.NonNull
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Factory
import org.hamcrest.Matcher

/**
 * A [Matcher] that tests whether the examined object implements a specific interface.
 *
 * @author Martin Albedinsky
 * @since 1.1
 *
 * @constructor
 * @param clazz
 */
internal class ImplementsMatcher<T>(private val clazz: Class<*>) : BaseMatcher<T>() {

    /**
     */
    companion object {

        /**
         * Creates a matcher that matches if examined object implements the specified interface [clazz].
         *
         * **Note that the specified [clazz] must be actually an interface and not a simple class,
         * otherwise an exception is thrown.**
         *
         * For example:
         *
         * `assertThat(object, implements(SomeInterface::class))`
         *
         * @param clazz The desired interface class to expect.
         * @param T Type of the object to be examined.
         * @return Matcher ready to be used.
         */
        @NonNull @JvmStatic @Factory
        fun <T> implements(@NonNull clazz: Class<*>): Matcher<T> {
            require(clazz.isInterface) { "Implements matcher can be used only for interfaces. Given '$clazz' is not an interface!" }
            return ImplementsMatcher<T>(clazz)
        }
    }

    /*
     */
    override fun matches(item: Any?): Boolean {
        return item != null && clazz.isAssignableFrom(item.javaClass)
    }

    /*
     */
    override fun describeTo(description: Description) {
        description.appendText("implements ").appendValue(clazz)
    }

    /*
     */
    override fun describeMismatch(item: Any?, description: Description) {
        when (item) {
            is Class<*> -> description.appendValue(item)
            null -> description.appendValue("null")
            else -> description.appendValue(item.javaClass)
        }

        description.appendText(" does not implement ").appendValue(clazz)
    }
}