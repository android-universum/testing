package universum.studios.android.testing.matcher

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class DeclarationMatchersTest : TestCase {

    @Test fun implements() {
        // Act + Assert:
        MatcherAssert.assertThat(DeclarationMatchers.implements<Any>(TestInterface::class.java),
            CoreMatchers.`is`(CoreMatchers.notNullValue()))
    }

    private interface TestInterface
}