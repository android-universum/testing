/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ImplementsMatcherTest : TestCase {

    @Test fun implementsForInterface() {
        // Act:
        val matcher = ImplementsMatcher.implements<Any>(TestInterface::class.java)

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not(ImplementsMatcher.implements<Any>(TestInterface::class.java))))
    }

    @Test(expected = IllegalArgumentException::class)
    fun implementsForClass() {
        // Act:
        ImplementsMatcher.implements<Any>(TestClass::class.java)
    }

    @Test fun matches() {
        // Arrange:
        val first = TestClass()
        val second = TestClassOther()

        // Act + Assert:
        assertThat(first, ImplementsMatcher.implements(TestInterface::class.java))
        assertThat(second, `is`(not(ImplementsMatcher.implements(TestInterface::class.java))))
    }

    private interface TestInterface
    private class TestClass : TestInterface
    private class TestClassOther
}