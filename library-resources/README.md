Testing-Resources
===============

This module contains utilities that may be used in **Android tests** to access **android resources**
like _styles, drawables, etc_ via their resource names.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atesting/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atesting/_latestVersion)

### Gradle ###

    compile "universum.studios.android:testing-resources:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [TestResources](https://bitbucket.org/android-universum/testing/src/main/library-resources/src/main/java/universum/studios/android/test/resource/TestResources.kt)
