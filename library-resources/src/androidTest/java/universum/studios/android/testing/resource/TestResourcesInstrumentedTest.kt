package universum.studios.android.testing.resource

import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class TestResourcesInstrumentedTest : AndroidTestCase() {

    @Test fun resourceIdentifierAnimation() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.ANIMATION,
                "test_anim"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierAnimator() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.ANIMATOR,
                "test_animator"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierAttribute() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.ATTRIBUTE,
                "testAttribute"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierBoolean() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.BOOLEAN,
                "test_bool"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierColor() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.COLOR,
                "test_color"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierDimension() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.COLOR,
                "test_color"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierDrawable() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.DRAWABLE,
                "test_drawable"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierInteger() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.INTEGER,
                "test_integer"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierLayout() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.LAYOUT,
                "test_layout"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierMenu() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.MENU,
                "test_menu"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierString() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.STRING,
                "test_string"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierStyle() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.STYLE,
                "Test.Style"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierTransition() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.TRANSITION,
                "test_transition"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierXml() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.XML,
                "test_xml"
            ),
            CoreMatchers.`is`(CoreMatchers.not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierUnknown() {
        // Act + Assert:
        MatcherAssert.assertThat(
            TestResources.resourceIdentifier(
                context(),
                TestResources.STYLE,
                "Unknown"
            ),
            CoreMatchers.`is`(TestResources.NO_RESOURCE)
        )
    }
}