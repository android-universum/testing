package universum.studios.android.testing.resource

import android.content.Context
import androidx.annotation.StringDef

/**
 * Convenience utility class which provides functionality that may be used to access **android resources**
 * like *styles, drawables, etc* via their resource names for a desired test [context][Context] via
 * [TestResources.resourceIdentifier] function.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object TestResources {

    /**
     * Constant that identifies no resource identifier.
     */
    const val NO_RESOURCE = 0

    /**
     * Type identifying **animation** resource that may be accessed as `R.anim.RESOURCE_NAME`.
     */
    const val ANIMATION = "anim"

    /**
     * Type identifying **animator** resource that may be accessed as `R.animator.RESOURCE_NAME`.
     */
    const val ANIMATOR = "animator"

    /**
     * Type identifying **attribute** resource that may be accessed as `R.attr.RESOURCE_NAME`.
     */
    const val ATTRIBUTE = "attr"

    /**
     * Type identifying **boolean** resource that may be accessed as `R.bool.RESOURCE_NAME`.
     */
    const val BOOLEAN = "bool"

    /**
     * Type identifying **color** resource that may be accessed as `R.color.RESOURCE_NAME`.
     */
    const val COLOR = "color"

    /**
     * Type identifying **dimension** resource that may be accessed as `R.dimen.RESOURCE_NAME`.
     */
    const val DIMENSION = "dimen"

    /**
     * Type identifying **drawable** resource that may be accessed as `R.drawable.RESOURCE_NAME`.
     */
    const val DRAWABLE = "drawable"

    /**
     * Type identifying **integer** resource that may be accessed as `R.integer.RESOURCE_NAME`.
     */
    const val INTEGER = "integer"

    /**
     * Type identifying **layout** resource that may be accessed as `R.layout.RESOURCE_NAME`.
     */
    const val LAYOUT = "layout"

    /**
     * Type identifying **menu** resource that may be accessed as `R.menu.RESOURCE_NAME`.
     */
    const val MENU = "menu"

    /**
     * Type identifying **string** resource that may be accessed as `R.string.RESOURCE_NAME`.
     */
    const val STRING = "string"

    /**
     * Type identifying **style** resource that may be accessed as `R.style.RESOURCE_NAME`.
     */
    const val STYLE = "style"

    /**
     * Type identifying **transition** resource that may be accessed as `R.transition.RESOURCE_NAME`.
     */
    const val TRANSITION = "transition"

    /**
     * Type identifying **xml** resource that may be accessed as `R.xml.RESOURCE_NAME`.
     */
    const val XML = "xml"

    /**
     * Defines an annotation for determining set of allowed resource types for [resourceIdentifier] function.
     */
    @StringDef(
        ANIMATION,
        ANIMATOR,
        ATTRIBUTE,
        BOOLEAN,
        COLOR,
        DRAWABLE,
        DIMENSION,
        INTEGER,
        LAYOUT,
        MENU,
        STRING,
        STYLE,
        TRANSITION,
        XML
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ResourceType

    /**
     * Resolves identifier for resource of the specified [resourceType] with the given [resourceName]
     * that is available in the given [context].
     *
     * @param context      Context used to access resources that are used to resolve the requested identifier.
     * @param resourceType Type of the resource for which to resolve its identifier.
     * @param resourceName Name of the resource for which to resolve its identifier.
     * @return Resolved identifier that may be used to obtain value of the desired resource from
     * resources or [NO_RESOURCE] if no such resource was found.
     *
     * @see Resources.getIdentifier
     */
    @JvmStatic fun resourceIdentifier(
        context: Context,
        @ResourceType resourceType: String,
        resourceName: String
    ): Int {
        return context.resources.getIdentifier(resourceName, resourceType, context.packageName)
    }
}