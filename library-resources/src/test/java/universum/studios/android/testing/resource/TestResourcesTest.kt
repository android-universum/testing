package universum.studios.android.testing.resource

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Ignore
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
@Ignore("Because fails when executed as part of whole test suite.")
class TestResourcesTest : AndroidTestCase() {

    @Test fun contract() {
        // Assert:
        assertThat(TestResources.NO_RESOURCE, `is`(0))
        assertThat(TestResources.ANIMATION, `is`("anim"))
        assertThat(TestResources.ANIMATOR, `is`("animator"))
        assertThat(TestResources.ATTRIBUTE, `is`("attr"))
        assertThat(TestResources.BOOLEAN, `is`("bool"))
        assertThat(TestResources.COLOR, `is`("color"))
        assertThat(TestResources.DIMENSION, `is`("dimen"))
        assertThat(TestResources.DRAWABLE, `is`("drawable"))
        assertThat(TestResources.INTEGER, `is`("integer"))
        assertThat(TestResources.LAYOUT, `is`("layout"))
        assertThat(TestResources.MENU, `is`("menu"))
        assertThat(TestResources.STRING, `is`("string"))
        assertThat(TestResources.STYLE, `is`("style"))
        assertThat(TestResources.TRANSITION, `is`("transition"))
        assertThat(TestResources.XML, `is`("xml"))
    }

    @Test fun resourceIdentifierAnimation() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.ANIMATION,
                "test_anim"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierAnimator() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.ANIMATOR,
                "test_animator"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierAttribute() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.ATTRIBUTE,
                "testAttribute"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierBoolean() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.BOOLEAN,
                "test_bool"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierColor() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.COLOR,
                "test_color"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierDimension() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.COLOR,
                "test_color"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierDrawable() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.DRAWABLE,
                "test_drawable"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierInteger() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.INTEGER,
                "test_integer"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierLayout() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.LAYOUT,
                "test_layout"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierMenu() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.MENU,
                "test_menu"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierString() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.STRING,
                "test_string"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierStyle() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.STYLE,
                "Test.Style"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierTransition() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.TRANSITION,
                "test_transition"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierXml() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.XML,
                "test_xml"
            ),
            `is`(not(TestResources.NO_RESOURCE))
        )
    }

    @Test fun resourceIdentifierUnknown() {
        // Act + Assert:
        assertThat(
            TestResources.resourceIdentifier(
                context,
                TestResources.STYLE,
                "Unknown"
            ),
            `is`(TestResources.NO_RESOURCE)
        )
    }
}