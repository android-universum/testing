Testing-View
===============

This module contains test cases which may be used to test **Android views/widgets**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atesting/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atesting/_latestVersion)

### Gradle ###

    compile "universum.studios.android:testing-view:${DESIRED_VERSION}@aar"

_depends on:_
[testing-core](https://bitbucket.org/android-universum/testing/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ViewTestCase](https://bitbucket.org/android-universum/testing/src/main/library-view/src/main/java/universum/studios/android/test/ViewTestCase.kt)
- [ViewTestCaseActivity](https://bitbucket.org/android-universum/testing/src/main/library-view/src/main/java/universum/studios/android/test/ViewTestCaseActivity.kt)
