package universum.studios.android.testing.view

import android.content.Context
import android.view.LayoutInflater
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class ViewTestCaseInstrumentedTest : ViewTestCase() {

    @Test fun perform() {
        // Act + Assert:
        MatcherAssert.assertThat(context, Is.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(context, Is.`is`(ApplicationProvider.getApplicationContext<Context>()))
        MatcherAssert.assertThat(viewContext, Is.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(viewContext, Is.`is`(CoreMatchers.not(context)))
        MatcherAssert.assertThat(layoutInflater, Is.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(layoutInflater, Is.`is`(LayoutInflater.from(viewContext)))
    }
}