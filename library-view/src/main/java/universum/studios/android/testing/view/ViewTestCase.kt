/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.view

import android.content.Context
import android.view.LayoutInflater
import androidx.annotation.StyleRes
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.robolectric.Robolectric
import universum.studios.android.testing.AndroidTestCase

/**
 * An [AndroidTestCase] implementation that may be used to group **suite of Android tests for view/widget**
 * that are to be executed either on a local *JVM* with shadowed *Android environment* or on an
 * emulated/real *Android Device*.
 *
 * **Default runner:** [AndroidJUnit4]
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class ViewTestCase(
    @StyleRes private val theme: Int = ViewTestCaseActivity.THEME_STYLE,
) : AndroidTestCase() {

    /**
     * Lazy context instance that may be used to instantiate the view under the test.
     *
     * This context is actually a [ViewTestCaseActivity] in a *CREATED* state with the theme style
     * resource specified for this test case via its [ViewTestCase(Int)][ViewTestCase] constructor.
     */
    protected val viewContext: Context by lazy {
        Robolectric.buildActivity(
            ViewTestCaseActivity::class.java,
            ViewTestCaseActivity.createIntent(theme)
        ).create().get()
    }

    /**
     * Lazy layout inflater instance obtained via [LayoutInflater.from] using the [viewContext]
     * as argument that may be used to inflate the view under the test.
     */
    protected val layoutInflater: LayoutInflater by lazy { LayoutInflater.from(viewContext) }
}