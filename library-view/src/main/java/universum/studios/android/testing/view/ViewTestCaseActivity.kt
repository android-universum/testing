/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.view

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.FrameLayout
import androidx.annotation.NonNull
import androidx.annotation.StyleRes
import androidx.annotation.VisibleForTesting

/**
 * Simple [Activity] that may be used in **Android view related tests** to provide view context with
 * a desired theme which could contain resource values necessary for the view under the test.
 *
 * If [ViewTestCaseActivity] is desired to be launched with a custom theme, an [Intent] created via
 * [ViewTestCaseActivity.createIntent] with that theme resource should be passed to the activity as
 * its extras intent.
 *
 * This activity by default uses an instance of [FrameLayout] as its content view.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see [ViewTestCase]
 */
open class ViewTestCaseActivity : Activity() {

    /**
     */
    companion object {

        /**
         * Style resource of the default theme used by the [ViewTestCaseActivity].
         */
        @JvmStatic @StyleRes
        val THEME_STYLE =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) android.R.style.Theme_Material_Light
            else android.R.style.Theme_Light

        /**
         * Extra key used to provide custom theme style resource for the [ViewTestCaseActivity] via [Intent].
         */
        @VisibleForTesting internal val EXTRA_THEME = "${ViewTestCaseActivity::class.java.name}.EXTRA.Theme"

        /**
         * Creates a new [Intent] for the [ViewTestCaseActivity] with the given [theme] style resource.
         *
         * **Note that this intent is not suited to be used to start [ViewTestCaseActivity] activity,
         * because it will only contain extras for the activity not the activity component necessary
         * to start the activity.**
         *
         * @param theme Style resource of the desired theme which should be applied to the
         *              [ViewTestCaseActivity] when created.
         * @return Intent with the theme resource presented.
         */
        @JvmStatic @NonNull
        fun createIntent(@StyleRes theme: Int): Intent = Intent().apply { putExtra(EXTRA_THEME, theme) }
    }

    /*
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(intent.getIntExtra(EXTRA_THEME, THEME_STYLE))

        setContentView(FrameLayout(this))
    }
}