package universum.studios.android.testing.view

import android.R.style
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ViewTestCaseActivityTest : AndroidTestCase() {

    @Test fun contract() {
        // Act + Assert:
        MatcherAssert.assertThat(
            ViewTestCaseActivity.THEME_STYLE, CoreMatchers.`is`(
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) style.Theme_Material_Light
                else style.Theme_Light
            )
        )
        MatcherAssert.assertThat(ViewTestCaseActivity.EXTRA_THEME,
            CoreMatchers.`is`("${ViewTestCaseActivity::class.java.name}.EXTRA.Theme"))
    }

    @Test fun createIntent() {
        // Act:
        val intent = ViewTestCaseActivity.createIntent(style.Theme_Black)

        // Assert:
        MatcherAssert.assertThat(intent, CoreMatchers.`is`(CoreMatchers.notNullValue()))
        MatcherAssert.assertThat(intent.extras?.size(), CoreMatchers.`is`(1))
        MatcherAssert.assertThat(
            intent.getIntExtra(ViewTestCaseActivity.EXTRA_THEME, -1),
            CoreMatchers.`is`(style.Theme_Black)
        )
    }
}