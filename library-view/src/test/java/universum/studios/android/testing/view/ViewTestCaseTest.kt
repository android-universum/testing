/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.testing.view

import android.content.Context
import android.view.LayoutInflater
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class ViewTestCaseTest : ViewTestCase() {

    @Test fun perform() {
        // Act + Assert:
        assertThat(context, `is`(notNullValue()))
        assertThat(context, `is`(ApplicationProvider.getApplicationContext<Context>()))
        assertThat(viewContext, `is`(notNullValue()))
        assertThat(viewContext, `is`(not(context)))
        assertThat(layoutInflater, `is`(notNullValue()))
        assertThat(layoutInflater, `is`(LayoutInflater.from(viewContext)))
    }
}